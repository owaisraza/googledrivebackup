package com.dawateislami.googlebackupdriveapp;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

import com.dawateislami.googledrivebackuptool.DriveTool;
import com.dawateislami.googledrivebackuptool.Utils.BackupNotificationManager;

public class SchedulerService extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        if(DriveTool.isServiceInvoke(this)){
            new BackupNotificationManager(this).notificationUitls(this, MyDriveActivity.class,
                    0,"Backup Notification","Please backup your saved data on google drive",
                    R.mipmap.ic_launcher,
                    0,
                    R.mipmap.ic_launcher,
                    BuildConfig.APPLICATION_ID,
                    getString(R.string.app_name));
            Log.e("BACKUP_SERVICE","invoke");
        }else{
            Log.e("BACKUP_SERVICE","Not invoke");
            DriveTool.setServiceInvoke(this);
        }

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
