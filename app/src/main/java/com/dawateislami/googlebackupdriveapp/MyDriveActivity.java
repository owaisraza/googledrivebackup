package com.dawateislami.googlebackupdriveapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.dawateislami.googledrivebackuptool.DriveTool;
import com.dawateislami.googledrivebackuptool.Utils.BackupNotificationManager;
import com.dawateislami.googledrivebackuptool.Utils.DriveCallback;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.api.services.drive.DriveScopes;

public class MyDriveActivity extends AppCompatActivity implements DriveCallback {

    private static final String TAG = "BackupToolActivity";
    private static final int REQUEST_CODE_SIGN_IN = 1;
    private Button btnPush,btnPull,btnCheck;
    private DriveTool driveTool;
    private String json  = "{\n" +
            "  \"type\":\"object\",\n" +
            "  \"properties\": {\n" +
            "    \"foo\": {\n" +
            "      \"type\": \"string\"\n" +
            "    },\n" +
            "    \"bar\": {\n" +
            "      \"type\": \"integer\"\n" +
            "    },\n" +
            "    \"baz\": {\n" +
            "      \"type\": \"boolean\"\n" +
            "    },\n" +
            "\t\"saz\": {\n" +
            "      \"type\": \"float\"\n" +
            "    }\n" +
            "  }\n" +
            "}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_my);

        requestSignIn();

        btnPull =  findViewById(R.id.btn_pull);
        btnPush = findViewById(R.id.btn_push);
        btnCheck = findViewById(R.id.btn_Check);
        btnPull.setOnClickListener(view -> {
            if(driveTool != null)
                driveTool.getDataToDrive();
            else Log.e(TAG,"fail restoration");
        });
        btnPush.setOnClickListener(view -> {
            if(driveTool != null)
                driveTool.createFileToDrive(json);
            else Log.e(TAG,"fail backup");
        });
        btnCheck.setOnClickListener(view -> {
            if(driveTool != null)
                driveTool.fileListToDrive();
            else Log.e(TAG,"fail backup");
        });

       new BackupNotificationManager(this).createJob(SchedulerService.class,15);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {

        if(requestCode == REQUEST_CODE_SIGN_IN){
            if (resultCode == Activity.RESULT_OK && resultData != null) {
                driveTool = new DriveTool(MyDriveActivity.this,"test.json",resultData,this);
                driveTool.fileListToDrive();
            }
        }
        super.onActivityResult(requestCode, resultCode, resultData);
    }

    private void requestSignIn() {
        Log.d(TAG, "Requesting sign-in");

        GoogleSignInOptions signInOptions =
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .requestScopes(new Scope(DriveScopes.DRIVE_APPDATA))
                        .build();
        GoogleSignInClient client = GoogleSignIn.getClient(this, signInOptions);

        // The result of the sign-in Intent is handled in onActivityResult.
        startActivityForResult(client.getSignInIntent(), REQUEST_CODE_SIGN_IN);
    }

    @Override
    public void onDriveConnected() {
        Log.e(TAG,driveTool.getFileId());
        Log.e(TAG,driveTool.getUserEmail());
        Log.e(TAG,driveTool.getSyncDate());
    }

    @Override
    public void showProgress(String backupType) {
        Log.e(TAG,"Show Progress");
    }

    @Override
    public void hideProgress(String backupType) {
        Log.e(TAG,"Hide Progress");
    }

    @Override
    public void isFileAvailable(boolean isExists) {
        if(isExists){
            Log.e(TAG ,"Successfully Found ");
        }else{
            Log.e(TAG ,"File Not Found ");
        }
    }

    @Override
    public void pullJsonContent(String content) {
        Log.e(TAG,content);
    }

    @Override
    public void pushJsonContent(boolean status) {

    }
}