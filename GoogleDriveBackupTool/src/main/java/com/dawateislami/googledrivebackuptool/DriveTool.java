package com.dawateislami.googledrivebackuptool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.Log;


import androidx.annotation.RequiresApi;

import com.dawateislami.googledrivebackuptool.Utils.BackupType;
import com.dawateislami.googledrivebackuptool.Utils.Common;
import com.dawateislami.googledrivebackuptool.Utils.Constants;
import com.dawateislami.googledrivebackuptool.Utils.DriveCallback;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;

public class DriveTool {

    private static final String TAG = "BackupToolLibrary";
    private Drive googleDriveService;
    private Activity activity;
    private String fileName;
    private DriveCallback callback;

    public DriveTool(Activity activity, String fileName, Intent signInResultIntent,DriveCallback callback) {
        this.activity = activity;
        this.fileName = fileName;
        this.callback = callback;
        handleSignInResult(signInResultIntent);
    }

    private void handleSignInResult(Intent result) {
        GoogleSignIn.getSignedInAccountFromIntent(result)
                .addOnSuccessListener(googleAccount -> {
                    Log.e(TAG, "Signed in as " + googleAccount.getEmail());
                    Common.setPreference(activity, Constants.USER_ID,googleAccount.getEmail());
                    Common.setPreference(activity, Constants.USER_NAME,googleAccount.getDisplayName());
                    Common.setPreference(activity, Constants.USER_THUMBNAIL,googleAccount.getPhotoUrl().toString());

                    // Use the authenticated account to sign in to the Drive service.
                    GoogleAccountCredential credential =
                            GoogleAccountCredential.usingOAuth2(
                                    activity, Collections.singleton(DriveScopes.DRIVE_APPDATA));
                    credential.setSelectedAccount(googleAccount.getAccount());


                    googleDriveService = new Drive.Builder(
                                    AndroidHttp.newCompatibleTransport(),
                                    new GsonFactory(),
                                    credential)
                                    .setApplicationName("Drive backup tool for application data")
                                    .build();

                    if(callback != null)
                        callback.onDriveConnected();

                })
                .addOnFailureListener(exception -> Log.e(TAG, "Unable to sign in.", exception));
    }

    private void writeFileLocally(String json){
        FileOutputStream fos = null;
        try {
            fos = activity.openFileOutput(fileName, Context.MODE_PRIVATE);
            fos.write(json.getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    public void createFileToDrive(String json){
        writeFileLocally(json);
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {
            File googleFile = null;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(callback != null)
                    callback.showProgress(BackupType.PUSH.getValue());
                if(!getFileId().isEmpty()){
                    deleteFileToDriveByName();
                    deleteFileToDriveById(getFileId());
                    delayTimeForDeletation();
                }
            }
            @Override
            protected String doInBackground(String... strings) {
                try {
                    File metadata = new File()
                            .setParents(Collections.singletonList(Constants.DRIVE_TYPE))
                            .setMimeType(Constants.FILE_TYPE)
                            .setName(fileName);
                    java.io.File filePath = new java.io.File(activity.getFilesDir() + "/" + fileName);
                    FileContent mediaContent = new FileContent(Constants.FILE_TYPE, filePath);
                    googleFile = googleDriveService.files().create(metadata,mediaContent).setFields("id").execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(googleFile != null ){
                    Common.setPreference(activity,Constants.FILE_ID,googleFile.getId());
                    return googleFile.getId();
                }else{
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(s != null){
                    if(callback != null){
                        Common.setPreference(activity,Constants.SYNC_DATE,String.valueOf(Common.dateLong()));
                        callback.pushJsonContent(true);
                        callback.hideProgress(BackupType.PUSH.getValue());
                        Log.e(TAG, "File Created...");
                    }else{
                        if(callback != null) {
                            callback.pushJsonContent(false);
                            callback.hideProgress(BackupType.PUSH.getValue());
                        }
                        Log.e(TAG, "No File Created");
                    }
                   // updateDataToDrive(s);
                } else{
                    if(callback != null) {
                        callback.pushJsonContent(false);
                        callback.hideProgress(BackupType.PUSH.getValue());
                    }
                    Log.e(TAG, "No File Created");
                }
            }
        };
        task.execute(json);
    }

    public void deleteFileToDriveById(String fileId){
        AsyncTask<String, Void, Void> task = new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... strings) {
                try{
                    googleDriveService.files().delete(strings[0]).execute();
                }catch (Exception e){
                    e.printStackTrace();
                }
                return null;
            }
        };
        task.execute(fileId);
    }

    public void updateDataToDrive(String json){
        AsyncTask<String, Void, String> task = new AsyncTask<String, Void, String>() {


            @Override
            protected String doInBackground(String... strings) {
                try {
                    File fileMetadata = new File();
                    fileMetadata.setName(fileName);
                    fileMetadata.setMimeType(Constants.FILE_TYPE);
                    fileMetadata.setParents(Collections.singletonList(Constants.DRIVE_TYPE));
                    ByteArrayContent contentStream = ByteArrayContent.fromString(Constants.FILE_TYPE, strings[0]);
                    Log.e(TAG , "File ID: " + getFileId());
                    File file = googleDriveService.files().update(getFileId(), fileMetadata,contentStream).setFields("id").execute();
                    Log.e(TAG, "File ID: " + file.getId() + ", File Name: " + file.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "Success";
            }

        };
        task.execute(json);
    }

    public void fileListToDrive(){
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(callback != null)
                    callback.showProgress(BackupType.LIST.getValue());
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (googleDriveService != null) {
                        FileList files = googleDriveService.files().list()
                                .setSpaces(Constants.DRIVE_TYPE)
                                .setFields("nextPageToken, files(id,name)")
                                .execute();
                        Log.e(TAG,files.getFiles().size() + "");
                        for (File file : files.getFiles()) {
                            if (fileName.equals(file.getName())) {
                                if (callback != null)
                                    callback.isFileAvailable(true);
                                Common.setPreference(activity,Constants.FILE_ID,file.getId());
                            } else {
                                if (callback != null)
                                    callback.isFileAvailable(false);
                            }
                            Log.e(TAG + "_LIST", "File ID: " + file.getId() + ", File Name: " + file.getName());
                        }
                    }
                    } catch(IOException e){
                        e.printStackTrace();
                    }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if(callback != null)
                    callback.hideProgress(BackupType.LIST.getValue());
            }
        };
        task.execute();

    }

    public void getDataToDrive(){
        AsyncTask<Void,Void,String> task = new AsyncTask<Void, Void, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                if(callback != null)
                    callback.showProgress(BackupType.PULL.getValue());

                if(getFileId().isEmpty()){
                    fileListToDrive();
                }
            }

            @Override
            protected String doInBackground(Void... voids) {
                File files ;
                String contents = "";
                try {
                    files = googleDriveService.files().get(getFileId()).execute();
                    Log.e(TAG + "_GET","File ID: " + files.getId() + ", File Name: " + files.getName());
                    if(fileName.equals(files.getName())) {
                        InputStream is = googleDriveService.files().get(getFileId()).executeMediaAsInputStream();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                        StringBuilder stringBuilder = new StringBuilder();
                        String line;

                        while ((line = reader.readLine()) != null) {
                            stringBuilder.append(line);
                        }
                        contents = stringBuilder.toString();
                        Log.e(TAG + "JSON", contents);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return contents;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(callback != null) {
                    Common.setPreference(activity,Constants.SYNC_DATE,String.valueOf(Common.dateLong()));
                    if(isJSONValid(s))
                        callback.pullJsonContent(s);
                    else callback.pullJsonContent("");
                    delayTimeForDeletation();
                    callback.hideProgress(BackupType.PULL.getValue());
                }
            }
        };
        task.execute();
    }


    private void deleteFileToDriveByName(){
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    if (googleDriveService != null) {
                        FileList files = googleDriveService.files().list()
                                .setSpaces(Constants.DRIVE_TYPE)
                                .setFields("nextPageToken, files(id,name)")
                                .execute();
                        for (File file : files.getFiles()) {
                            if (fileName.equals(file.getName())) {
                                googleDriveService.files().delete(file.getId()).execute();
                               Log.e(TAG,"Delete File ID :" + file.getId());
                            }else{
                                Log.e(TAG,"File Name :" + file.getName());
                            }
                        }
                    }
                } catch(IOException e){
                    e.printStackTrace();
                }
                return null;
            }
        };
        task.execute();
    }

    public String getFileId(){
        return Common.getPreference(activity, Constants.FILE_ID, "");
    }

    public String getUserEmail(){
        return Common.getPreference(activity, Constants.USER_ID, "");
    }

    public String getUserName(){
        return Common.getPreference(activity, Constants.USER_NAME, "");
    }

    public String getUserPhoto(){
        return Common.getPreference(activity, Constants.USER_THUMBNAIL, "");
    }

    public String getSyncDate(){
        return Common.getPreference(activity, Constants.SYNC_DATE, "0");
    }



    public String getNotifyDays(){
        return Common.getPreference(activity, Constants.NOTIFY_DAYS, "15");
    }

    public static void setServiceInvoke(Context mContext){
        Common.setPreference(mContext, Constants.IS_SERVICE_CREATE, "Yes");
    }
    public static void setServiceNotInvoke(Context mContext){
        Common.setPreference(mContext, Constants.IS_SERVICE_CREATE, "No");
    }
    public static boolean isServiceInvoke(Context mContext){
        return Common.getPreference(mContext,Constants.IS_SERVICE_CREATE,"").equals("Yes");
    }

    public static boolean isJSONValid(String jsonInString) {
        try {
            new Gson().fromJson(jsonInString, Object.class);
            return true;
        } catch(com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }

    private void delayTimeForDeletation(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG,"Delay 1.5 sec");
            }
        }, 1500);
    }

}
