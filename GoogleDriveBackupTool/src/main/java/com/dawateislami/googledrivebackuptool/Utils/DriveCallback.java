package com.dawateislami.googledrivebackuptool.Utils;

public interface DriveCallback {
    void onDriveConnected();
    void showProgress(String backupType);
    void hideProgress(String backupType);
    void isFileAvailable(boolean isExists);
    void pullJsonContent(String content);
    void pushJsonContent(boolean status);


}
