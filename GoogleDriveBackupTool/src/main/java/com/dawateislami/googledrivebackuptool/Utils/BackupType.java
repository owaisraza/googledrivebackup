package com.dawateislami.googledrivebackuptool.Utils;

public enum BackupType {

    PUSH("PULL"), PULL("PUSH"), LIST("ALL"), SEARCH("SEARCH");
    private String value;

    private BackupType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
