package com.dawateislami.googledrivebackuptool.Utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.dawateislami.googledrivebackuptool.DriveTool;

import static android.content.Context.JOB_SCHEDULER_SERVICE;

public class BackupNotificationManager {

    private static final String TAG = "BackupToolNotification";
    private Context mContext;
    private long periodicTime;
    private int days;

    public BackupNotificationManager(Context mContext){
        this.mContext = mContext;
    }

    public void createJob(Class<?> service,int days){
        JobScheduler  jobScheduler = (JobScheduler) mContext.getSystemService(JOB_SCHEDULER_SERVICE);
        Common.setPreference(mContext,Constants.NOTIFY_DAYS,String.valueOf(days));
        if(days > 0){
            ComponentName componentName = new ComponentName(mContext, service);
            JobInfo.Builder builder = new JobInfo.Builder(Constants.JOB_BACKUP_ID,componentName);
            long repeatingTime = repeatingTimeInMilis(days);
            builder.setPeriodic(repeatingTime);
            builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NONE);
            builder.setPersisted(true);
            JobInfo jobInfo = builder.build();

            assert jobScheduler != null;
            int jobStatus = jobScheduler.schedule(jobInfo);
            if (jobStatus != JobScheduler.RESULT_SUCCESS)
                Log.d(TAG, "Job not initialize");
            else
                Log.d(TAG, "Job initialize");
            DriveTool.setServiceNotInvoke(mContext);
        }else{
            assert jobScheduler != null;
            jobScheduler.cancel(Constants.JOB_BACKUP_ID);
            //Common.setPreference(mContext, Constants.IS_SERVICE_CREATE, "");
        }
    }

    public void notificationUitls(Context mContext,Class<?> landedClass,int activityId,String tittle,String message,
                                  int smallImage,int bigImage,int transparentImage,String notificationChannelID,
                                  String notificationChannelName){

        Intent resultIntent = new Intent(mContext, landedClass);
        resultIntent.putExtra("ACTIVITY_ID",activityId);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(mContext, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationChannel mChannel = null;
        NotificationCompat.Builder  mBuilder = null;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            mChannel= new NotificationChannel(notificationChannelID, notificationChannelName, importance);
        }

        mBuilder = new NotificationCompat.Builder(mContext,notificationChannelID);
        mBuilder.setContentTitle(tittle);
        mBuilder.setSmallIcon(smallImage);
        if(bigImage != 0)
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), bigImage));
        if(message != null)
            mBuilder.setContentText(message);

        mBuilder.setContentIntent(viewPendingIntent);
        mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        mBuilder.setAutoCancel(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            mBuilder.setSmallIcon(transparentImage);

        NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(mContext);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);
        }

        mNotificationManager.notify(activityId, mBuilder.build());
    }

    private long repeatingTimeInMilis(int days){
        return days * 24 * 60 * 60 * 1000;
       // return days  * 60 * 1000;
    }


}
