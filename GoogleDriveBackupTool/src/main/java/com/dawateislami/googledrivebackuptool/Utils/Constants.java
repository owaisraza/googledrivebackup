package com.dawateislami.googledrivebackuptool.Utils;

public class Constants {

    public static final String PREFS_NAME = "googleDriveBackupPreferences";
    public static final String FILE_TYPE = "application/json"; // "text/plain";
    public static final String DRIVE_TYPE = "appDataFolder";
    public static final String DATE_FORMAT = "MMM dd, yyyy";
    public static final String FILE_ID = "file_id";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_THUMBNAIL = "user_thumbnail";
    public static final String SYNC_DATE = "sync_date";
    public static final String NOTIFY_DAYS = "notify_days";
    public static final String IS_SERVICE_CREATE = "service_create";
    public static final int JOB_BACKUP_ID = 1211;
    public static final long DEFAULT_TIME = 900000;


}
